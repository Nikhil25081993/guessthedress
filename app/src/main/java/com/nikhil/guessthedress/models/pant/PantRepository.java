package com.nikhil.guessthedress.models.pant;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.os.AsyncTask;

import com.nikhil.guessthedress.models.WardrobeRoomDatabase;


import java.util.List;

/**
 * Created by NiCK on 6/14/2018.
 */

public class PantRepository {


    private PantDao mpantDao;
    private LiveData<List<Pant>> mAllPants;

    PantRepository(Application application) {
        WardrobeRoomDatabase db = WardrobeRoomDatabase.getDatabase(application);
        mpantDao = db.pantDao();
        mAllPants = mpantDao.getAllPant();
    }


    LiveData<List<Pant>> getAllPant() {
        return mAllPants;
    }

    public void insert (Pant pant) {
        new insertAsyncTask(mpantDao).execute(pant);
    }


    private static class insertAsyncTask extends AsyncTask<Pant, Void, Void> {

        private PantDao mAsyncTaskDao;

        insertAsyncTask(PantDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Pant... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

}
