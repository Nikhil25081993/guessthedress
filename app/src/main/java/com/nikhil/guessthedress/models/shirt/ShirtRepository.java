package com.nikhil.guessthedress.models.shirt;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.nikhil.guessthedress.models.WardrobeRoomDatabase;

import java.util.List;

/**
 * Created by admin on 6/13/2018.
 */

public class ShirtRepository {

    private ShirtDao mshirtDao;
    private LiveData<List<Shirt>> mAllShirts;

    ShirtRepository(Application application) {
        WardrobeRoomDatabase db = WardrobeRoomDatabase.getDatabase(application);
        mshirtDao = db.shirtDao();
        mAllShirts = mshirtDao.getAllShirt();
    }

    LiveData<List<Shirt>> getAllShirt() {
        return mAllShirts;
    }

    public void insert (Shirt shirt) {
        new insertAsyncTask(mshirtDao).execute(shirt);
    }


    private static class insertAsyncTask extends AsyncTask<Shirt, Void, Void> {

        private ShirtDao mAsyncTaskDao;

        insertAsyncTask(ShirtDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Shirt... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}
