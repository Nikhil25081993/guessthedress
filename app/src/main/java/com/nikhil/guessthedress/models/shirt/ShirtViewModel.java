package com.nikhil.guessthedress.models.shirt;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

/**
 * Created by admin on 6/13/2018.
 */

public class ShirtViewModel extends AndroidViewModel {

    private ShirtRepository shirtRepository;
    private LiveData<List<Shirt>> mAllShirts;

    public ShirtViewModel (Application application) {
        super(application);
        shirtRepository = new ShirtRepository(application);
        mAllShirts = shirtRepository.getAllShirt();
    }

    public LiveData<List<Shirt>> getAllShirts() { return mAllShirts; }

    public void insert(Shirt shirt) { shirtRepository.insert(shirt); }


}
