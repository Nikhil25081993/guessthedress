package com.nikhil.guessthedress.models.pant;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;


import java.util.List;

/**
 * Created by NiCK on 6/14/2018.
 */

@Dao
public interface PantDao {

        @Insert
        void insert(Pant pant);

        @Query("DELETE FROM pant_table")
        void deleteAll();

        @Query("SELECT * from pant_table ORDER BY pant ASC")
        LiveData<List<Pant>> getAllPant();

        @Query("SELECT * from pant_table")
        List<Pant> getAllPants();

}
