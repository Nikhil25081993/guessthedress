package com.nikhil.guessthedress.models.pant;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by NiCK on 6/14/2018.
 */
@Entity(tableName = "pant_table")
public class Pant {


        @PrimaryKey
        @NonNull
        @ColumnInfo(name = "pant")
        private String pant;

        public Pant(@NonNull String pant) {this.pant = pant;}

        @NonNull
        public String getPant(){return this.pant;}


}
