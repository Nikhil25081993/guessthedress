package com.nikhil.guessthedress.models;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.nikhil.guessthedress.models.pant.Pant;
import com.nikhil.guessthedress.models.pant.PantDao;
import com.nikhil.guessthedress.models.shirt.Shirt;
import com.nikhil.guessthedress.models.shirt.ShirtDao;

/**
 * Created by admin on 6/13/2018.
 */


@Database(entities = {Shirt.class, Pant.class}, version = 1)
public abstract class WardrobeRoomDatabase extends RoomDatabase {
    public abstract ShirtDao shirtDao();
    public abstract PantDao pantDao();

    private static WardrobeRoomDatabase INSTANCE;


    public static WardrobeRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (WardrobeRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            WardrobeRoomDatabase.class, "wardrobe_database")
                            .allowMainThreadQueries()
                            .build();

                }
            }
        }
        return INSTANCE;
    }


}
