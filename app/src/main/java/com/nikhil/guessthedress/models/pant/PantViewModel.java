package com.nikhil.guessthedress.models.pant;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;


import java.util.List;

/**
 * Created by NiCK on 6/14/2018.
 */

public class PantViewModel extends AndroidViewModel{

    private PantRepository pantRepository;
    private LiveData<List<Pant>> mAllPant;

    public PantViewModel (Application application) {
        super(application);
        pantRepository = new PantRepository(application);
        mAllPant = pantRepository.getAllPant();
    }

    public LiveData<List<Pant>> getAllPant() { return mAllPant; }

    public void insert(Pant pant) { pantRepository.insert(pant); }


}
