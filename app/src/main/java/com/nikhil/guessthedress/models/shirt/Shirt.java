package com.nikhil.guessthedress.models.shirt;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by admin on 6/13/2018.
 */
@Entity(tableName = "shirt_table")

public class Shirt {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "shirt")
    private String shirt;

    public Shirt(@NonNull String shirt) {this.shirt = shirt;}

    @NonNull
    public String getShirt(){return this.shirt;}


}
