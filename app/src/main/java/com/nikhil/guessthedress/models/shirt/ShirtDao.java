package com.nikhil.guessthedress.models.shirt;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by admin on 6/13/2018.
 */

@Dao
public interface ShirtDao {

    @Insert
    void insert(Shirt shirt);

    @Query("DELETE FROM shirt_table")
    void deleteAll();

    @Query("SELECT * from shirt_table ORDER BY shirt ASC")
    LiveData<List<Shirt>> getAllShirt();


    @Query("SELECT * from shirt_table")
    List<Shirt> getShirts();



}
