package com.nikhil.guessthedress.utils;

public interface DialogButtonsListener {
    void onNegativeButtonPress();
    void onPositiveButtonPress();

}
