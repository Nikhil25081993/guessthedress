package com.nikhil.guessthedress;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.nikhil.guessthedress.models.pant.Pant;
import com.nikhil.guessthedress.models.pant.PantViewModel;
import com.nikhil.guessthedress.models.shirt.Shirt;
import com.nikhil.guessthedress.models.WardrobeRoomDatabase;
import com.nikhil.guessthedress.models.shirt.ShirtViewModel;
import com.nikhil.guessthedress.utils.Constants;
import com.nikhil.guessthedress.utils.DialogButtonsListener;
import com.nikhil.guessthedress.utils.ImageCompressor;
import com.nikhil.guessthedress.utils.UIUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DemoActivity extends AppCompatActivity {

    List<String> shirtImages;
    List<String> pantImages;
    private ViewPager mViewPagerShirts;
    private CustomPagerAdapter mShirtAdapter;
    private ViewPager mViewPagerPants;
    private CustomPagerAdapter mPantsAdapter;
    private Button btShirt;
    private Button btPant;

    private final int RQ_GALLERY = 1;
    private final int RQ_CAMERA = 2;
    private boolean pantSelected, shirtSelected;
    private File file;
    private Button btRandomize;
    private int previousPant = 0, previosShirt = 0;
    private static final String DATABASE_NAME = "shirt_database";
    private WardrobeRoomDatabase database;
    private LiveData<List<Shirt>> allShirt;
    private ShirtViewModel model;
    private static final String TAG = "DemoActivity";
    private ShirtViewModel shirtModel;
    private PantViewModel pantModel;
    private List<Shirt> shirts;
    private List<Pant> pant;
    int newShirt = 1, newPant = 1;


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);

//        final ProgressDialog dialog = new ProgressDialog(this);
//        dialog.setMessage("Loading images please wait");
//        dialog.setCancelable(false);
//        dialog.show();


        database = WardrobeRoomDatabase.getDatabase(this);

        shirts = database.shirtDao().getShirts();
        pant = database.pantDao().getAllPants();

        if (shirts.size() <= 0 && pant.size() <= 0) {

            populateWithDummyData();

        }




        shirtImages = new ArrayList<>();
        pantImages = new ArrayList<>();


        for (int i = 0; i < shirts.size(); i++) {
            shirtImages.add(shirts.get(i).getShirt());
        }
        for (int i = 0; i < pant.size(); i++) {
            pantImages.add(pant.get(i).getPant());
        }

        shirtModel = ViewModelProviders.of(this).get(ShirtViewModel.class);
        pantModel = ViewModelProviders.of(this).get(PantViewModel.class);



//        shirtModel.getAllShirts().observe(this, new Observer<List<Shirt>>() {
//            @Override
//            public void onChanged(@Nullable final List<Shirt> shirts) {
//
//                Log.d("TAGGGGG", "onChanged: ------->shirts" + shirts);
//
//                if (shirts.size() > 0) {
//                    shirtImages.clear();
//                    for (int i = 0; i < shirts.size(); i++) {
//                        shirtImages.add(shirts.get(i).getShirt());
//                    }
//                } else {
//                    mViewPagerShirts.setVisibility(View.INVISIBLE);
//                }
//
//
//                pantModel.getAllPant().observe(DemoActivity.this, new Observer<List<Pant>>() {
//                    @Override
//                    public void onChanged(@Nullable List<Pant> pants) {
//
//                        dialog.dismiss();
//
//                        Log.d("TAGGGGG", "onChanged: ------->pants" + pants);
//                        if (pants.size() > 0) {
//                            pantImages.clear();
//                            for (int i = 0; i < pants.size(); i++) {
//                                pantImages.add(pants.get(i).getPant());
//                            }
//                        } else {
//                            mViewPagerPants.setVisibility(View.GONE);
//                        }
//
////                        shirtImages = Constants.getShirts();
////                        pantImages = Constants.getPants();
//
//
//                    }
//                });
//
//
//            }
//
//        });




        shirtModel.getAllShirts().observe(DemoActivity.this, new Observer<List<Shirt>>() {
            @Override
            public void onChanged(@Nullable List<Shirt> shirts) {

                Log.d(TAG, "onChanged: shirt changed" );

                shirtImages.clear();

                for (int i = 0; i < shirts.size(); i++) {

                    shirtImages.add(shirts.get(i).getShirt());

                }


                mShirtAdapter.notifyDataSetChanged();



            }
        });



        pantModel.getAllPant().observe(DemoActivity.this, new Observer<List<Pant>>() {
            @Override
            public void onChanged(@Nullable List<Pant> pants) {

                Log.d(TAG, "onChanged: pant changed" );

                pantImages.clear();

                for (int i = 0; i < pants.size(); i++) {

                    pantImages.add(pants.get(i).getPant());

                }

                mPantsAdapter.notifyDataSetChanged();
            }
        });

        Log.d(TAG, "onCreate:kcabsca");

//
//
//        Shirt shirt = new Shirt("shirt_new");
//        model.insert(shirt);




        mShirtAdapter = new CustomPagerAdapter(DemoActivity.this, shirtImages);
        mPantsAdapter = new CustomPagerAdapter(DemoActivity.this, pantImages);

        btShirt = (Button) findViewById(R.id.bt_addShirt);
        btPant = (Button) findViewById(R.id.bt_addPant);
        btRandomize = (Button) findViewById(R.id.bt_randomize);
        mViewPagerShirts = (ViewPager) findViewById(R.id.vp_shirts);
        mViewPagerPants = (ViewPager) findViewById(R.id.vp_pants);
        mViewPagerShirts.setAdapter(mShirtAdapter);
        mViewPagerPants.setAdapter(mPantsAdapter);



        btRandomize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (shirtImages.size() > 1) {

                    while ( newShirt == previosShirt){
                        newShirt = new Random().nextInt(shirtImages.size());
                    }
                    previosShirt = newShirt;
                }

                Log.d("TAG", "onClick: -----shirt> " + previosShirt);
                if (pantImages.size() > 1) {


                    while (newPant == previousPant){
                        newPant = new Random().nextInt(pantImages.size());
                    }
                    previousPant = newPant;

                }
                Log.d("TAG", "onClick: -----shirt> " + previousPant);

                mViewPagerShirts.setCurrentItem(previosShirt);
                mViewPagerPants.setCurrentItem(previousPant);

            }
        });

        btShirt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shirtSelected = true;
                showPicker();
            }
        });


        btPant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pantSelected = true;
                showPicker();

            }
        });



    }

    private void populateWithDummyData() {


        Shirt shirt = new Shirt("https://5.imimg.com/data5/YJ/BO/MY-10973479/mens-designer-casual-shirt-500x500.jpg");
        Pant pant = new Pant("https://djxuk3gezpm3t.cloudfront.net/media/catalog/product/cache/8/image/040ec09b1e35df139433887a97daa66f/h/o/hope-law-trouser-black-front_7.jpg");
        database.shirtDao().insert(shirt);
        database.pantDao().insert(pant);






    }


    private void showPicker() {

        AlertDialog.Builder builder = new AlertDialog.Builder(DemoActivity.this);
        builder.setItems(new String[]{"Gallery", "Camera"}, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        if (Build.VERSION.SDK_INT >= 23) {
                            // Here, thisActivity is the current activity
                            if (ContextCompat.checkSelfPermission(DemoActivity.this,
                                    Manifest.permission.READ_EXTERNAL_STORAGE)
                                    != PackageManager.PERMISSION_GRANTED) {

                                // Should we show an explanation?
                                if (ActivityCompat.shouldShowRequestPermissionRationale(DemoActivity.this,
                                        Manifest.permission.READ_EXTERNAL_STORAGE)) {

                                    final AlertDialog alertDialog = UIUtils.buildAlertDialog(DemoActivity.this, "", "IQlive online needs Storage permission to access photos on your device", "Okay", true,
                                            new DialogButtonsListener() {
                                                @Override
                                                public void onNegativeButtonPress() {

                                                }

                                                @Override
                                                public void onPositiveButtonPress() {
                                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                        requestPermissions(
                                                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                                                RQ_GALLERY);
                                                    }
                                                }
                                            });
                                    alertDialog.show();

                                } else {

                                    // No explanation needed, we can request the permission.

                                    requestPermissions(
                                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                            RQ_GALLERY);

                                    // MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
                                    // app-defined int constant. The callback method gets the
                                    // result of the request.
                                }
                            } else {
                                Intent galleryIntent = new Intent(Intent.ACTION_PICK);
                                galleryIntent.setType("image/*");
                                startActivityForResult(galleryIntent, RQ_GALLERY);
                            }
                        } else {
                            Intent galleryIntent = new Intent(Intent.ACTION_PICK);
                            galleryIntent.setType("image/*");
                            startActivityForResult(galleryIntent, RQ_GALLERY);
                        }

                        break;
                    case 1:
                        // Camera needs both CAMERA and READ_EXTERNAL_STORAGE permission. Storage permission is needed to store
                        // picture temporarily.
                        if (Build.VERSION.SDK_INT >= 23) {
                            // Here, thisActivity is the current activity
                            if ((ContextCompat.checkSelfPermission(DemoActivity.this,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    != PackageManager.PERMISSION_GRANTED)) {

                                // Should we show an explanation?
                                if (ActivityCompat.shouldShowRequestPermissionRationale(DemoActivity.this,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                                    final AlertDialog alertDialog = UIUtils.buildAlertDialog(DemoActivity.this, "", "IQlive online needs Storage permission to access photos on your device", "Okay", true, new DialogButtonsListener() {
                                        @Override
                                        public void onNegativeButtonPress() {

                                        }

                                        @Override
                                        public void onPositiveButtonPress() {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(
                                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                                        RQ_CAMERA);
                                            }
                                        }
                                    });
                                    alertDialog.show();

                                } else {

                                    // No explanation needed, we can request the permission.

                                    requestPermissions(
                                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                            RQ_CAMERA);

                                    // MY_PERMISSIONS_REQUEST_CAMERA is an
                                    // app-defined int constant. The callback method gets the
                                    // result of the request.
                                }
                            } else {
//                                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                                        try {
////                                            cameraUri = Uri.fromFile(createTempFile());
//                                            cameraUri = FileProvider.getUriForFile(getContext(),
//                                                    getActivity().getApplicationContext().getPackageName() + ".provider", createTempFile());
//                                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, cameraUri);
//
//                                        } catch (IOException e) {
//                                            e.printStackTrace();
//                                        }
//                                        startActivityForResult(cameraIntent, RQ_CAMERA);
                                openCameraIntent();
                            }
                        } else {
                            openCameraIntent();
                        }

                        break;
                }
            }
        });
        builder.setTitle("Select Picture");
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case RQ_GALLERY:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK);
                    galleryIntent.setType("image/*");
                    startActivityForResult(galleryIntent, RQ_GALLERY);
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                break;
            case RQ_CAMERA:

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

//                    // permission was granted, yay! Do the
//                    // contacts-related task you need to do.
//                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    try {
//                        cameraUri = Uri.fromFile(createTempFile());
//                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, cameraUri);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    startActivityForResult(cameraIntent, RQ_CAMERA);
                    openCameraIntent();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            Uri imageUri;
            switch (requestCode) {
                case RQ_GALLERY:
                    imageUri = data.getData();
                    if (shirtSelected) {
                        shirtModel.insert(new Shirt(imageUri.toString()));
                        shirtSelected=false;
                        shirtImages.add(imageUri != null ? imageUri.toString() : null);
                        mShirtAdapter.notifyDataSetChanged();

                    } else if (pantSelected) {
                        pantModel.insert(new Pant(imageUri.toString()));
                        pantSelected=false;
                        pantImages.add(imageUri != null ? imageUri.toString() : null);
                        mPantsAdapter.notifyDataSetChanged();

                    }
                    break;
                case 100:
//                    imageUri = cameraUri;
                    if (data != null && data.getExtras() != null) {
                        Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");
//                            file = new File(getRealPathFromURI(getContext(), imageUri));
                        file = createFile(imageBitmap);
                        imageUri = Uri.fromFile(file);

                        if (shirtSelected) {
                            shirtModel.insert(new Shirt(imageUri.toString()));
                            shirtSelected=false;
                            shirtImages.add(imageUri != null ? imageUri.toString() : null);
                            mShirtAdapter.notifyDataSetChanged();

                        } else if (pantSelected) {
                            pantModel.insert(new Pant(imageUri.toString()));
                            pantSelected=false;
                            pantImages.add(imageUri != null ? imageUri.toString() : null);
                            mPantsAdapter.notifyDataSetChanged();

                        }

                    }

                    break;
                case RQ_CAMERA:
//                    imageUri = cameraUri;

                    break;

            }
        }

    }


    private File createFile(Bitmap bitmap) {


        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, bytes);

//you can create a new file name "test.jpg" in sdcard folder.
        File f = new File(Environment.getExternalStorageDirectory()
                + File.separator + "test.jpg");
        try {
            f.createNewFile();
            //write the bytes in file
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());

// remember close de FileOutput
            fo.close();
            return f;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }

    private void openCameraIntent() {
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(this.getPackageManager()) != null) {
            startActivityForResult(pictureIntent,
                    100);
        }
    }


    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


    @NonNull
    private File compressImage(Uri croppedUri) throws IOException {
        Bitmap compressImage = new ImageCompressor(getApplicationContext()).compressImage(croppedUri
                .toString());
        File fileToUpload = getOutputMediaFile("tempImage");// write the file
        byte byteArray[] = bitmapToByte(compressImage);
        FileOutputStream fos = new FileOutputStream(fileToUpload); // writing for temporary reason for creating compressedBitmap;
        fos.write(byteArray);
        fos.close();
        return fileToUpload;
    }

    public File getOutputMediaFile(String fileName) {
        File mediaStorageDir = this.getCacheDir();
        File mediaFile = new File(mediaStorageDir.getAbsolutePath() + File.separator + fileName + ".jpg");
        return mediaFile;
    }

    public byte[] bitmapToByte(Bitmap bitmap) {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }


    class CustomPagerAdapter extends PagerAdapter {

        Context mContext;
        List<String> Images;
        LayoutInflater mLayoutInflater;

        public CustomPagerAdapter(Context context, List<String> images) {
            mContext = context;
            Images = images;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return Images.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.iv_singleWardrobeImage);
            Glide.with(DemoActivity.this).load(Images.get(position)).into(imageView);

            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }
}
